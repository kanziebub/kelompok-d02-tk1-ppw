from django.apps import AppConfig


class GamingForumConfig(AppConfig):
    name = 'gaming_forum'
