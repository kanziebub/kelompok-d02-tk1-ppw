from django.db import models

# Create your models here.

class GamingPost(models.Model) :
	name    = models.CharField(max_length=30)
	title   = models.CharField(max_length=30)
	content = models.TextField(max_length=1000)
	image   = models.ImageField(blank=True, null=True, upload_to="gaming")

	def __str__(self):
		return self.title