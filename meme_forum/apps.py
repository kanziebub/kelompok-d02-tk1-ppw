from django.apps import AppConfig


class MemeForumConfig(AppConfig):
    name = 'meme_forum'
