from django.shortcuts import render, redirect
from .models import MemePost
from .forms import MemeForm


# Create your views here.
def Post(request):
    model = MemePost.objects.all().order_by('-id')
    count = model.count()
    response = {
        'posts' : model,
        'count' : count,
    }
    return render(request, 'forum_meme_overview.html', response)


def addPost(request):
    form = MemeForm()
    if (request.method == 'POST'):
        form = MemeForm(request.POST, request.FILES)
        if (form.is_valid()):
            model = MemePost()
            model.name = form.cleaned_data['name']
            model.title = form.cleaned_data['title']
            model.content = form.cleaned_data['content']
            model.image = form.cleaned_data['image']

            if (model.name == ''):
                model.name = 'Anonymous'

            model.save()
        return redirect('/meme-forum/')
    context = {
        'form' : form,
    }
    return render(request, 'forum_meme_addpost.html', context)

def detailPost(request, pk):
    model = MemePost.objects.all().get(id=pk)
    response = {"model" : model}
    return render(request, "forum_meme_details.html", response)
