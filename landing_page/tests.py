from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Email

# Create your tests here.

class TestLandingPage(TestCase):
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_template_is_exist(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landing_page.html')

    def test_func_is_used(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_is_created(self):
        Email.objects.create(email="example@abc.com")
        count = Email.objects.all().count()
        self.assertEqual(count, 1)

    def test_print_model(self):
        email = Email.objects.create(email="example@abc.com")
        self.assertEqual(str(email), email.email)
    
    def test_can_save_POST_request(self):
        response = self.client.post('', data={'email': 'abc@abc.com'})
        count_request = Email.objects.all().count()
        self.assertEqual(count_request, 1)
        
        response = index(HttpRequest())
        html_response = response.content.decode('utf8')
        self.assertIn('1', html_response)