from django import forms
from .models import Email

class Email_Form(forms.ModelForm):
    class Meta:
        model = Email
        fields = ['email']

    error_messages = {
        'required' : 'Please Type'
    }

    email = forms.EmailField(widget=forms.EmailInput(attrs={
            'placeholder' : 'imvvibu@ui.ac.id',
            'maxlength' : 50,
            'type' : 'email',
            'required' : True
        }))