from django.shortcuts import render
from .forms import Email_Form
from .models import Email

# Create your views here.

def index(request):
    form = Email_Form(request.POST or None)
    if (form.is_valid and request.method =='POST'):
        form.save()
    email_count = Email.objects.all().count()
    response = {
        'email_form' : Email_Form,
        'email_count': email_count,
    }
    return render(request, 'landing_page.html', response)