from django.test import TestCase, Client
from django.http import HttpRequest, request, response
from django.urls import reverse, resolve

from . import apps, forms, views
from .models import PostWibu

# Create your tests here.
class ForumAnimeTests (TestCase):
    # ---------- Testing Apps ----------
    def test_app_anime_forum(self):
        appname = apps.AnimeForumConfig.name
        self.assertEquals(appname, 'anime_forum')

    # ---------- Testing URLs ----------
    def test_url_anime_forum_overview(self):
        response = self.client.get('/anime-forum/')
        self.assertEquals(response.status_code, 200)
        
    def test_url_anime_forum_add_post(self):
        response = self.client.get('/anime-forum/add-post')
        self.assertEquals(response.status_code, 200)

    def test_url_anime_forum_addpost(self):
        PostWibu.objects.create(
            title='Simp for Cu',
            name='kanziebub',
            content='I can not picture myself getting married to anyone but cu.',
        )
        response = self.client.get('/anime-forum/1/details')
        self.assertEquals(response.status_code, 200)

    # ---------- Testing Models ----------
    def test_model_str(self):
        PostWibu.objects.create(
            title='Levi can step on me',
            name='NotPetra',
            content="Smexy clean freak can kick the life outta me.",
        )
        id_X = PostWibu.objects.all()[0].id
        post = PostWibu.objects.get(id=id_X)
        self.assertEquals(str(post), 'Levi can step on me')


    def test_model_create_post_without_image_upload(self):
        PostWibu.objects.create(
            title='Simp for Cu',
            name='kanziebub',
            content='I can not picture myself getting married to anyone but cu.',
        )
        num = PostWibu.objects.all().count()
        self.assertEquals(num, 1)

    def test_model_create_post_without_name(self):
        PostWibu.objects.create(
            title='I Love Traps',
            content="Astolfo is best boy AND best girl. Can't change my mind.",
        )
        num = PostWibu.objects.filter(name='Anonymous').count()
        self.assertEquals(num, 1)
    

    # ---------- Testing Views ----------
    def test_view_forum_anime_overview_response(self):
        response = Client().get('/anime-forum/')
        self.assertEquals(response.status_code, 200)

    def test_view_forum_anime_create_post_response(self):
        response = Client().post(
            '/anime-forum/add-post', 
            data={
                'title': 'The only King I would Serve',
                'name': 'Enkidu',
                'content': 'I love you Gilgamesh',
            })
        self.assertEquals(response.status_code, 302)
    
    def test_view_forum_anime_create_post_counted(self):
        Client().post(
            '/anime-forum/add-post', 
            data={
                'title': 'Poor Jeanne',
                'name': 'Gudao',
                'content': 'I hate F##### so muchhh, how could they do that to a war hero??!!',
            })
        num = PostWibu.objects.filter(name='Gudao').count()
        self.assertEquals(num, 1)

    def test_view_forum_anime_create_anon_post_counted(self):
        Client().post(
            '/anime-forum/add-post', 
            data={
                'title': 'Apocrypha',
                'content': 'I love teh Fate Series, but Apocrypha is just a No No. The MC is so bland I- UGGG',
            })
        num = PostWibu.objects.filter(title='Apocrypha').count()
        self.assertEquals(num, 1)





    