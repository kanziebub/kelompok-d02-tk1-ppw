from django.shortcuts import render, redirect
from .forms import AddPost
from .models import PostWibu

# Create your views here.

def overview (request):
    model = PostWibu.objects.all().order_by('-id')
    tally = model.count()
    context = {
        'posts' : model,
        'tally' : tally,
    }
    return render(request, 'forum_anime_overview.html', context)

def post_create (request):
    form = AddPost()
    if (request.method == 'POST'):
        form = AddPost(request.POST, request.FILES)
        if (form.is_valid()):
            model = PostWibu()
            model.name = form.cleaned_data['name']
            model.title = form.cleaned_data['title']
            model.content = form.cleaned_data['content']
            model.image = form.cleaned_data['image']

            if (model.name == ''):
                model.name = 'Anonymous'

            model.save()
        return redirect('/anime-forum/')
    context = {
        'form' : form,
    }
    return render(request, 'forum_anime_addpost.html', context)

def post_details (request, idx):
    post = PostWibu.objects.get(id=idx)

    context = {
        'post' : post,
    }
    return render(request, 'forum_anime_details.html', context)