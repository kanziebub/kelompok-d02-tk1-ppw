from django.apps import AppConfig


class AnimeForumConfig(AppConfig):
    name = 'anime_forum'
