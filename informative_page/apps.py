from django.apps import AppConfig


class InformativePageConfig(AppConfig):
    name = 'informative_page'
