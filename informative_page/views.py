from django.shortcuts import render
from .models import Feedback
from .forms import Feedback_Form

def informative_page(request):
    form = Feedback_Form(request.POST or None)
    if (form.is_valid and request.method =='POST'):
        form.save()
    feedback_count = Feedback.objects.all().count()
    response = {'feedback_form': Feedback_Form, 'feedback_count': feedback_count}
    return render(request, 'informative_page.html', response)