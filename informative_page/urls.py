from django.contrib import admin
from django.urls import path
from .views import informative_page

urlpatterns = [
    path('informative-page', informative_page, name='informative_page'),
]