from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import informative_page
from .models import Feedback

class InformativePageTest(TestCase):

    def test_url_informative_page_is_exist(self):
        response = Client().get('/informative-page')
        self.assertEqual(response.status_code, 200)

    def test_using_informative_page_template(self):
        response = Client().get('/informative-page')
        html = response.content.decode('utf8')
        self.assertIn("FORUM RULES", html)
        self.assertIn("ABOUT US", html)
        self.assertIn("help us improve", html)
        self.assertIn("CONTACT US", html)
        self.assertTemplateUsed(response, 'informative_page.html')

    def test_using_informative_page_func(self):
        found = resolve('/informative-page')
        self.assertEqual(found.func, informative_page)
        
    def test_model_can_create_new_feedback(self):
        Feedback.objects.create(feedback='Mantap gan')
        counting_all_available_feedback = Feedback.objects.all().count()
        self.assertEqual(counting_all_available_feedback, 1)

    def test_feedback_saved_and_displayed(self):
        response = Client().post('/informative-page', {'feedback': 'Mantaappp'})
        self.assertEqual(Feedback.objects.all().count(), 1)
        self.assertEqual(response.status_code, 200)
        html = response.content.decode('utf8')
        self.assertIn("1", html)
